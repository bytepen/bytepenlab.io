---
layout: post
title:  "Resources"
date:   2020-03-29 22:57:46 -0500
categories: ['education']
excerpt_separator: <!--more-->
---

Interested in becoming a security researcher or want to further your skills? Just want to learn more about computers? I'm working on compiling a list of educational material and tools you can use to get started or for future reference!

<!--more-->

You can find this page here:

[Resources](https://bytepen.gitlab.io/edu/resources)
