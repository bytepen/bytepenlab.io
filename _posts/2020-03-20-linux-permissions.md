---
layout: post
title:  "Linux Permissions"
date:   2020-03-20 12:00:00 -0500
categories: ['education']
excerpt_separator: <!--more-->
---

If you're not as familiar with Linux Permissions as you would like to be, or would just like to read up on some of the ways they can be tricky, I encourage you to check this out!

<!--more-->

I have created a GitBook that goes over each of the Linux Permissions and a handful of gotcha's that can spring up when dealing with them.

I have also created a Docker Image to help you learn about Linux Permissions with hands-on challenges.

To get more information about this project, check it out on GitLab Pages at the link below:

[Linux Permissions](http://bytepen.gitlab.io/education/linux-permissions/)
