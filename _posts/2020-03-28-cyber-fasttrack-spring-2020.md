---
layout: post
title:  "Cyber FastTrack: Spring 2020"
date:   2020-03-28 21:35:22 -0500
categories: ['ctf']
excerpt_separator: <!--more-->
---

I participated in the Cyber FastTrack: Spring 2020 Capture the Flag challenge and decided to publish a Page going over some of the challenges.

<!--more-->

Overall, I found this CTF to be pretty challenging, but in a way that was very educational and got me to try out a handful of new tools that I might not have normally
tried. Primarily, it was my first encounter with [NetworkMiner](https://www.netresec.com/?page=NetworkMiner) and [Autopsy](https://www.sleuthkit.org/autopsy/).

NetworkMiner does not come installed with Kali and takes a little bit of work. NetworkMiner is incredibly helpful if you have a .pcap file and would like to pull a lot of information (such as credentials) out easily without having to dig through Wireshark.

Autopsy comes with Kali, but the version is from 2010 and is very outdated. The new version of Autopsy can be installed without TOO much work and I highly recommend doing so. Autopsy allows you to examine disk images and easily extract information. The newer version also has a registry viewer, which was incredibly useful and super easy to use.

I've only scratched the surface with both of these tools, but this CTF has made them an invaluable part of my toolset and a mandatory install on any Kali box I'll be using in the future.

You can find the write-ups for challenges I was able to complete as well as some other information about the Cyber FastTrack: Spring 2020 event at the link below:

[Cyber FastTrack: Spring 2020 CTF Write-Up](https://bytepen.gitlab.io/ctf/cyber-fasttrack-spring-2020)
