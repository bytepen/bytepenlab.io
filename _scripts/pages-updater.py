#!/usr/bin/python3

import urllib.request
import json
import os

project_json = json.loads(urllib.request.urlopen("https://gitlab.com/api/v4/groups/bytepen/projects?include_subgroups=true").read())
subgroup_json = json.loads(urllib.request.urlopen("https://gitlab.com/api/v4/groups/bytepen/subgroups").read())
groups = []
projects = []

for p in project_json:
    attribs = ["name", "web_url", "path_with_namespace", "last_activity_at"]
    d = {}
    
    if p["pages_access_level"] in ["public", "enabled"] and p["name"] != "bytepen.gitlab.io":
        for a in attribs:
            d[a] = p[a]

            d["pages_url"] = p["path_with_namespace"].replace("bytepen/", "https://bytepen.gitlab.io/")

        projects.append(d)

for g in subgroup_json:
    attribs = ["name", "full_path", "path"]
    d = {}

    for a in attribs:
        d[a] = g[a]
    
    groups.append(d)

os.makedirs("_pages", exist_ok=True)

with open("_pages/all.md", "w") as pf:
    pf.write("---\nlayout: page\ntitle: All\npermalink: /all/\n---\n")
    for g in sorted(groups, key = lambda i: i["name"]):
        with open(f"_pages/{g['path']}.md", "w") as gf:
            gf.write(f"---\nlayout: page\ntitle: {g['name']}\npermalink: /{g['path']}/\n---\n")
            pf.write(f"\n## {g['name']}\n")
            for p in sorted(projects, key = lambda i: i["last_activity_at"], reverse=True):
                if p['path_with_namespace'].startswith(g['full_path']):
                    line = f"* [{p['name']}]({p['pages_url']}) ([Last Activity: {p['last_activity_at'][:10]}]({p['web_url']}))\n"
                    pf.write(line)
                    gf.write(line)
